
void prereqBinCheck()
{

/* see if graphical notifier tool is installed */
	int showNotifierWindow = 1;
	const char *notifierBin = "/usr/local/bin/dvnotifier";
	if(access(notifierBin, X_OK) != 0) /* if dvnotifier is not accessible... */
	{
		/* print error messages */
		fprintf(stdout, "\n DeskView Notifier tool could not be found!\n");
		fprintf(stdout, " Further messages and warnings will go to stdout only.\n");
		fprintf(stdout, " This is a non-fatal error. You can ignore it.\n\n");
		fflush(stdout);
		
		showNotifierWindow = 0; /* disable use of dvnotifier for all subsequent errors */
	}
	
/* test for xdotool binary */
	const char *xdotoolBin = "/usr/bin/xdotool";
	if(access(xdotoolBin, X_OK) != 0)
	{
		char cmdBuffer[256];
		
		if(showNotifierWindow == 1)
		{
			/* compile error message strings and send to graphical notifier */
			snprintf(cmdBuffer, sizeof(cmdBuffer), "dvnotifier -w -c -m \"xdotool not found! Copy/Paste will not work.\" &");
			
			/* run popup window */
			system(cmdBuffer);
		}
		
		/* send messages to standard error */
		snprintf(cmdBuffer, sizeof(cmdBuffer), "\n xdotool not found! Copy/Paste will not work.\n\n");
		
		fprintf(stderr, cmdBuffer);
		fflush(stderr);
	}
	
/* test for mlocate binary */
	const char *locateBin = "/usr/bin/locate";
	if(access(locateBin, X_OK) != 0)
	{
		char cmdBuffer[256];
		
		if(showNotifierWindow == 1)
		{
			snprintf(cmdBuffer, sizeof(cmdBuffer), "dvnotifier -e -c -m \"mlocate/locate not found!\n\nThis is a fatal error. Program has quit.\" &");
			system(cmdBuffer);
		}
		
		snprintf(cmdBuffer, sizeof(cmdBuffer), "\n mlocate/locate not found!\n This is a fatal error. Program has quit.\n\n");
		fprintf(stderr, cmdBuffer);
		fflush(stderr);
		
		/* missing mlocate/locate binary is a showstopper - quit immediately */
		exit(0);
	}
}

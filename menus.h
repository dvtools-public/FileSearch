void installMenus()
{

/* grab background color for later use with widgets */
	Pixel menu_bg, menu_fg, menu_text;
	XtVaGetValues(menuBar, XmNbackground, &menu_bg, NULL);
	XtVaGetValues(mainWin, XmNforeground, &menu_fg, NULL);
	XtVaGetValues(mainWin, XmNbackground, &menu_text, NULL);
	
/* custom arrow pixmap for cascading submenus */
	Pixmap arrow_bitmap = XCreateBitmapFromData(XtDisplay(topLevel),
	RootWindowOfScreen(XtScreen(topLevel)), cascade_arrow3_bits, cascade_arrow3_width, cascade_arrow3_height);
	
	
/* pull down form to hold the sub-entries for the File menu */
	fileMenuPane = XmCreatePulldownMenu(menuBar, "fileMenuPane", NULL, 0);
	XtVaSetValues(fileMenuPane,
		XmNshadowThickness, 1,
		XmNmarginHeight, 1,
		XmNmarginWidth, 1,
		XmNtopShadowColor, menu_fg,
		XmNbottomShadowColor, menu_fg,
	NULL);

/* XmCascadeButton for the visible File entry on the menuBar
   It posts the drop down fileMenuPane when activated  */
	Widget fileMenu = XtVaCreateManagedWidget("File", xmCascadeButtonWidgetClass, menuBar, 
		XmNsubMenuId, fileMenuPane,
		XmNmnemonic, 'F',
		XmNshadowThickness, 1,
		XmNtopShadowColor, menu_fg,
		XmNbottomShadowColor, menu_fg,
		XmNmarginHeight, 2,
	NULL);
	

	Widget fileQuit = XtVaCreateManagedWidget("fileQuit", xmPushButtonWidgetClass, fileMenuPane,
		XmNmnemonic, 'Q',
		XmNmarginWidth, 6,
		XmNmarginHeight, 4,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNaccelerator, XmRString, "Ctrl<Key>Q", 4,
		XtVaTypedArg, XmNacceleratorText, XmRString, "Ctrl + Q", 4,
		XtVaTypedArg, XmNlabelString, XmRString, "Quit", 4,
	NULL);
	XtAddCallback(fileQuit, XmNactivateCallback, instantQuit, NULL);
	
	
/* beginning of all Options related menus */
	optMenuPane = XmCreatePulldownMenu(menuBar, "optMenuPane", NULL, 0);
	XtVaSetValues(optMenuPane,
		XmNshadowThickness, 1,
		XmNmarginHeight, 1,
		XmNmarginWidth, 1,
		XmNtopShadowColor, menu_fg,
		XmNbottomShadowColor, menu_fg,
	NULL);
	
	Widget optMenu = XtVaCreateManagedWidget("Options", xmCascadeButtonWidgetClass, menuBar, 
		XmNsubMenuId, optMenuPane,
		XmNmnemonic, 'O',
		XmNshadowThickness, 1,
		XmNtopShadowColor, menu_fg,
		XmNbottomShadowColor, menu_fg,
	NULL);

	optEntry0 = XtVaCreateManagedWidget("optEntry0", xmPushButtonWidgetClass, optMenuPane,
		XmNmnemonic, 'C',
		XmNmarginWidth, 6,
		XmNmarginHeight, 4,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNaccelerator, XmRString, "Ctrl<Key>C", 4,
		XtVaTypedArg, XmNacceleratorText, XmRString, "Ctrl + C", 4,
		XtVaTypedArg, XmNlabelString, XmRString, "Case Sensitivity (Disabled)", 4,
	NULL);
	XtAddCallback(optEntry0, XmNactivateCallback, toggleCaseSensitivity, NULL);
	

/* beginning of all Window related menus */
	winMenuPane = XmCreatePulldownMenu(menuBar, "winMenuPane", NULL, 0);
	XtVaSetValues(winMenuPane,
		XmNshadowThickness, 1,
		XmNmarginHeight, 1,
		XmNmarginWidth, 1,
		XmNtopShadowColor, menu_fg,
		XmNbottomShadowColor, menu_fg,
	NULL);

	Widget winMenu = XtVaCreateManagedWidget("Window", xmCascadeButtonWidgetClass, menuBar, 
		XmNsubMenuId, winMenuPane,
		XmNmnemonic, 'W',
		XmNshadowThickness, 1,
		XmNtopShadowColor, menu_fg,
		XmNbottomShadowColor, menu_fg,
	NULL);

	winEntry0 = XtVaCreateManagedWidget("winEntry0", xmPushButtonWidgetClass, winMenuPane,
		XmNmnemonic, 'S',
		XmNmarginWidth, 6,
		XmNmarginHeight, 4,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNaccelerator, XmRString, "Ctrl<Key>S", 4,
		XtVaTypedArg, XmNacceleratorText, XmRString, "Ctrl + S", 4,
		XtVaTypedArg, XmNlabelString, XmRString, "Top of Stack", 4,
	NULL);
	XtAddCallback(winEntry0, XmNactivateCallback, toggleAlwaysOnTop, NULL);
	
	winEntry1 = XtVaCreateManagedWidget("winEntry1", xmPushButtonWidgetClass, winMenuPane,
		XmNmnemonic, 'N',
		XmNmarginWidth, 6,
		XmNmarginHeight, 4,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNaccelerator, XmRString, "Ctrl<Key>N", 4,
		XtVaTypedArg, XmNacceleratorText, XmRString, "Ctrl + N", 4,
		XtVaTypedArg, XmNlabelString, XmRString, "Launch New Instance", 4,
	NULL);
	XtAddCallback(winEntry1, XmNactivateCallback, newInstance, NULL);
	

/* beginning of all Help related menus */
	Widget helpMenuPane = XmCreatePulldownMenu(menuBar, "helpMenuPane", NULL, 0);
	XtVaSetValues(helpMenuPane,
		XmNshadowThickness, 1,
		XmNmarginHeight, 1,
		XmNmarginWidth, 1,
		XmNtopShadowColor, menu_fg,
		XmNbottomShadowColor, menu_fg,
	NULL);

	Widget helpMenu = XtVaCreateManagedWidget("Help", xmCascadeButtonWidgetClass, menuBar, 
		XmNsubMenuId, helpMenuPane,
		XmNmnemonic, 'H',
		XmNshadowThickness, 1,
		XmNtopShadowColor, menu_fg,
		XmNbottomShadowColor, menu_fg,
	NULL);

	Widget helpEntry0 = XtVaCreateManagedWidget("helpEntry0", xmPushButtonWidgetClass, helpMenuPane,
		XmNmnemonic, 'A',
		XmNmarginWidth, 6,
		XmNmarginHeight, 4,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNaccelerator, XmRString, "Ctrl<Key>A", 4,
		XtVaTypedArg, XmNacceleratorText, XmRString, "Ctrl + A", 4,
		XtVaTypedArg, XmNlabelString, XmRString, "About", 4,
	NULL);
	
	Widget helpEntry1 = XtVaCreateManagedWidget("helpEntry1", xmPushButtonWidgetClass, helpMenuPane,
		XmNmnemonic, 'U',
		XmNmarginWidth, 6,
		XmNmarginHeight, 4,
		XmNshadowThickness, 1,
		XmNbackground, menu_bg,
		XtVaTypedArg, XmNaccelerator, XmRString, "Ctrl<Key>U", 4,
		XtVaTypedArg, XmNacceleratorText, XmRString, "Ctrl + U", 4,
		XtVaTypedArg, XmNlabelString, XmRString, "Usage", 4,
	NULL);

/* let the menu bar know we want help on the right */
	XtVaSetValues(menuBar,
		XmNmenuHelpWidget, helpMenu,
	NULL);
}

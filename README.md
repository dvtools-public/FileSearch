### File Search

A text based file search utility for X11. This is a graphical wrapper around mlocate/locate. The default color scheme is optional and can be commented out before compiling. Edit xresources.h to change hard-coded colors and fonts.


![screenshot](screenshots/dvsearch_sample.png)


(Searching for files with .efi extension)

#### Primary Features

- Easily search for file names or extensions with optional case sensitivity.
- Easily update mlocate database.

#### Secondary Features

- Ctrl + C - Toggle case sensitivity.
- Ctrl + S - Keep window at top of stack above all others.
- Ctrl + N - Launch new instance.
- Ctrl + Q - Quit.

#### Known Defects

Updating the database requires root or sudo access. File Search can prompt for a password using my 'dvsudo' tool but I haven't made it public yet.


Help menus for About and Usage are unfinished.

#### Requirements

- Motif 2.3.8
- X11/Xlib
- libXpm
- libXinerama
- [dvnotifier](https://gitlab.com/dvtools-public/Notifier) (optional)
- xdotool (optional - only required for Copy/Paste)
- mlocate

#### Build Instructions

Compile:

```
make clean && make
```

Run:

```
./dvsearch
```

Install:

```
sudo make install
```

Uninstall:

```
sudo make uninstall
```


#### License

This software is distributed free of charge under the BSD Zero Clause license.
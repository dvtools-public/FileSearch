
/* system/POSIX header files */
#include <stdio.h>
#include <stdlib.h> /* system() */
#include <string.h> /* strcpy() */
#include <unistd.h> /* access() */
#include <sys/types.h>
#include <sys/stat.h> /* stat() */
#include <pthread.h> /* POSIX multi-threading */
#include <time.h> /* time(), strftime() */

#include <Xm/Xm.h> /* motif core */
#include <Xm/MainW.h> /* high level manager form within topLevel */
#include <Xm/RowColumn.h> /* menu bar */
#include <Xm/CascadeB.h> /* cascading menus in menuBar */
#include <Xm/PushB.h> /* buttons in menus and main form */
#include <Xm/Form.h>
#include <Xm/Frame.h>
#include <Xm/Label.h>
#include <Xm/Text.h> /* text input boxes */
#include <Xm/Separator.h>
#include <Xm/Protocols.h> /* keyboard shortcuts */
#include <Xm/ScrolledW.h> /* holds left disk list radios */

#include <X11/Xlib.h>
#include <X11/Intrinsic.h> /* XInitThreads() for X11 multi-tthreading */
#include <X11/Shell.h>
#include <X11/Xatom.h> /* get motif WM properties */
#include <X11/extensions/Xinerama.h> /* window centering */
#include <X11/xpm.h> /* pixmaps */

/* for passing data to functions */
typedef struct 
{
	Widget textWidget;
	Widget textWidget2;
	Widget statusLabel;
} WidgetData;


/* make widgets globally accessible */
Widget topLevel, mainWin,
menuBar,
	fileMenuPane, 
	optMenuPane,
		optEntry0,
	tempMenuPane, 
	winMenuPane, 
		winEntry0,
		winEntry1,
masterForm,

textWidget,

resultsForm,
textWidget2,

scrolledWin0,
tasksRowColumn,

statusLabel,
dateLabel;



char *textWidgetString;
char caseSensitivity[64] = "--ignore-case "; 


#include "icon_data.h" /* custom bitmaps */
#include "callbacks.h" /* callbacks for widgets */
#include "prereqs.h" /* make sure needed binaries are installed */
#include "menus.h" /* create menubar entries */
#include "xresources.h" /* fallback X11 resources */


int main(int argc, char *argv[])
{
/* quick check to see if prerequisite binaries are installed */
	prereqBinCheck();

/* initialize x11 multi-threading - required to XFlush from pthreads */
	XInitThreads();

/* define xt context */
	XtAppContext app;
	
	topLevel = XtVaAppInitialize(&app, "dvsearch", NULL, 0, &argc, argv, fb_xres, NULL);
	/* set some initial resources for topLevel*/
	XtVaSetValues(topLevel,
		XmNtitle, "File Search",
		XmNiconName, " File Search ",
		
		XmNminWidth, 600,
		XmNminHeight, 360,
		
		XmNwidth, 800,
		XmNheight, 600,
	NULL);

/* XmMainWindow as high level container widget */
	mainWin = XtVaCreateManagedWidget("mainWin", xmMainWindowWidgetClass, topLevel, NULL);
	XtVaSetValues(mainWin,
		XmNshadowThickness, 1,
	NULL);
	
/* create a menu bar by brute forcing a XmRowColumn widget with XmNrowColumnType */
	menuBar = XtVaCreateManagedWidget("menuBar", xmRowColumnWidgetClass, mainWin, 
		XmNrowColumnType, XmMENU_BAR,
		XmNshadowThickness, 1,
		XmNtopAttachment, XmATTACH_FORM,
		XmNleftAttachment, XmATTACH_FORM,
		XmNrightAttachment, XmATTACH_FORM,
		XmNmarginHeight, 4,
	NULL);

/* too long, couldn't read - split off into menus.h */
	installMenus();

/* create another container form below the menu bar */
	masterForm = XtVaCreateManagedWidget("masterForm", xmFormWidgetClass, mainWin,
		XmNshadowThickness, 0,
		XmNshadowType, XmSHADOW_IN,
		/* pin the main form underneath the menu bar */
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, menuBar,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 0,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 0,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 0,
	NULL);
	
	
	Pixmap refresh_pixmap = XCreateBitmapFromData(XtDisplay(topLevel), 
	RootWindowOfScreen(XtScreen(topLevel)), refresh_bits, refresh_width, refresh_height);
	
/* update database button */
	Widget updateButton = XtVaCreateManagedWidget("Update Database", xmPushButtonWidgetClass, masterForm,
		XmNalignment, XmALIGNMENT_BEGINNING,
		XmNhighlightThickness, 0,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 9,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 8,
		XmNmarginLeft, 10,
		XmNmarginRight, 6,
		XmNlabelType, XmPIXMAP_AND_STRING,
		XmNpixmapPlacement, XmPIXMAP_RIGHT,
		XmNpixmapTextPadding, 6,
		XmNarmPixmap, refresh_pixmap,
	NULL);
	
	
	Pixmap search_pixmap = XCreateBitmapFromData(XtDisplay(topLevel), 
	RootWindowOfScreen(XtScreen(topLevel)), mag_glass_bits, mag_glass_width, mag_glass_height);
	
/* search button */
	Widget searchButton = XtVaCreateManagedWidget("Search", xmPushButtonWidgetClass, masterForm,
		XmNalignment, XmALIGNMENT_BEGINNING,
		XmNhighlightThickness, 0,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 9,
		XmNrightAttachment, XmATTACH_WIDGET,
		XmNrightWidget, updateButton,
		XmNrightOffset, 6,
		XmNmarginLeft, 12,
		XmNmarginRight, 6,
		XmNlabelType, XmPIXMAP_AND_STRING,
		XmNpixmapPlacement, XmPIXMAP_RIGHT,
		XmNpixmapTextPadding, 6,
		XmNarmPixmap, search_pixmap,
	NULL);
	

/* main search input textbox */
	textWidget = XtVaCreateManagedWidget("textWidget", xmTextWidgetClass, masterForm,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 8,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 9,
		XmNshadowThickness, 2,
		XmNrightAttachment, XmATTACH_WIDGET,
		XmNrightWidget, searchButton,
		XmNrightOffset, 5,
		XmNcursorPositionVisible, 0,
		
		XmNmarginHeight, 5,
		XmNmarginWidth, 7,
		
		XmNeditMode, XmSINGLE_LINE_EDIT,
		XmNhighlightThickness, 1,
		XmNshadowThickness, 2,
		XmNcursorPositionVisible, 0,
		XmNautoShowCursorPosition, 0,
		XmNblinkRate, 400,
	NULL);
	
	XtAddCallback(textWidget, XmNfocusCallback, textWidgetGainedFocus, textWidget);
	XtAddCallback(textWidget, XmNlosingFocusCallback, textWidgetLostFocus, textWidget);
	
	Dimension textW_h;
	XtVaGetValues(textWidget, XmNheight, &textW_h, NULL);
	
	XtVaSetValues(searchButton, XmNheight, textW_h - 1, NULL);
	XtVaSetValues(updateButton, XmNheight, textW_h - 1, NULL);
	
	Pixel masterContainer_bg, masterContainer_fg;
	XtVaGetValues(masterForm, XmNbackground, &masterContainer_bg, NULL);
	XtVaGetValues(masterForm, XmNforeground, &masterContainer_fg, NULL);
	
	
/* create right click popup */
	Widget popupMenu = XmCreatePopupMenu(textWidget, "popupMenu", NULL, 0);
	XtVaSetValues(popupMenu,
		XmNshadowThickness, 1,
		XmNmarginHeight, 1,
		XmNmarginWidth, 1,
		XmNtopShadowColor, masterContainer_fg,
		XmNbottomShadowColor, masterContainer_fg,
	NULL);
	
/* menu entry for paste */
	Widget pasteText = XmCreateCascadeButton(popupMenu, " Paste ", NULL, 0);
	XtManageChild(pasteText);
	XtVaSetValues(pasteText,
		XmNmarginWidth, 8,
		XmNmarginHeight, 6,
	NULL);
	XtAddCallback(pasteText, XmNactivateCallback, pasteCallback, textWidget);

/* menu entry for copy */
	Widget copyText = XmCreateCascadeButton(popupMenu, " Copy ", NULL, 0);
	XtManageChild(copyText);
	XtVaSetValues(copyText,
		XmNmarginWidth, 8,
		XmNmarginHeight, 6,
	NULL);
	XtAddCallback(copyText, XmNactivateCallback, copyCallback, textWidget);
	
/* event handler for the right click */
	XtAddEventHandler(textWidget, ButtonPressMask, False, rightClickCallback, popupMenu);
	
	
/* create frame around output text widget */
	Widget textSWFrame = XtVaCreateManagedWidget("textWidget2", xmFrameWidgetClass, masterForm,
		XmNshadowThickness, 0,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 9,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 8,
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, textWidget,
		XmNtopOffset, 7,
		XmNshadowThickness, 0,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 45,
		XmNshadowType, XmSHADOW_ETCHED_IN,
	NULL);


/* create output text widget */	
	Arg args[3];
	int n = 0;
	
	XtSetArg(args[n], XmNscrollingPolicy, XmAUTOMATIC); n++;
	XtSetArg(args[n], XmNscrollBarDisplayPolicy, XmAS_NEEDED); n++;
	XtSetArg(args[n], XmNvisualPolicy, XmVARIABLE); n++;
	
	Widget textWidget2 = XmCreateScrolledText(textSWFrame, "textWidget2", args, n);
	XtVaSetValues(textWidget2,
		XmNeditMode, XmMULTI_LINE_EDIT,
		XmNcursorPositionVisible, False,
		XmNeditable, False,
		XmNhighlightThickness, 0,
		XmNshadowThickness, 2,
		XmNautoShowCursorPosition, True,
	NULL);
	XtManageChild(textWidget2);
	
	
	/* create right click popup */
	Widget popupMenu2 = XmCreatePopupMenu(textWidget2, "popupMenu", NULL, 0);
	XtVaSetValues(popupMenu2,
		XmNshadowThickness, 1,
		XmNmarginHeight, 1,
		XmNmarginWidth, 1,
		XmNtopShadowColor, masterContainer_fg,
		XmNbottomShadowColor, masterContainer_fg,
	NULL);
	
	/* menu entry for copy */
	Widget copyText2 = XmCreateCascadeButton(popupMenu2, " Copy Selected Text ", NULL, 0);
	XtManageChild(copyText2);
	XtVaSetValues(copyText2,
		XmNmarginWidth, 8,
		XmNmarginHeight, 6,
	NULL);
	XtAddCallback(copyText2, XmNactivateCallback, copyCallback, textWidget2);
	
	/* event handler for the right click */
	XtAddEventHandler(textWidget2, ButtonPressMask, False, rightClickCallback, popupMenu2);
	
	Widget sep = XtVaCreateManagedWidget("sep", xmSeparatorWidgetClass, masterForm,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 0,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 0,
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, textWidget2,
		XmNtopOffset, 8,
	NULL);
	
	Widget statusLabel = XtVaCreateManagedWidget("statusLabel", xmLabelWidgetClass, masterForm,
		XmNalignment, XmALIGNMENT_BEGINNING,
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, sep,
		XmNtopOffset, 2,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 6,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 2,
		XtVaTypedArg, XmNlabelString, XmRString, "Ready.", 4,
	NULL);
	
	/* find out when db was last updated */
	const char* get_last_updated = "stat -c '%Y' /var/lib/mlocate/mlocate.db";
	char* last_updated_str = fetchOutputFromCmd(get_last_updated);

	time_t last_updated_time = (time_t)atol(last_updated_str);
	free(last_updated_str);

	/* convert time_t to struct */
	struct tm *tm_info = localtime(&last_updated_time);

	/* string format time */
	char timeBuffer[512];
	strftime(timeBuffer, sizeof(timeBuffer), "Database Last Updated: %a %b %d, %Y at %I:%M:%S %p", tm_info);

	XmString lastUpdatedString = XmStringCreateLocalized(timeBuffer);
	
	
	Widget dateLabel = XtVaCreateManagedWidget("dateLabel", xmLabelWidgetClass, masterForm,
		XmNalignment, XmALIGNMENT_BEGINNING,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 6,
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, sep,
		XmNtopOffset, 2,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 2,
		XmNlabelString, lastUpdatedString,
	NULL);
	
	/* free string */
	XmStringFree(lastUpdatedString);
	
	Widget sep2 = XtVaCreateManagedWidget("sep2", xmSeparatorWidgetClass, masterForm,
		XmNorientation, XmVERTICAL,
		XmNrightAttachment, XmATTACH_WIDGET,
		XmNrightWidget, dateLabel,
		XmNrightOffset, 2,
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, sep,
		XmNtopOffset, -2,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 0,
	NULL);
	
	WidgetData widgetData;
	widgetData.textWidget = textWidget;
	widgetData.textWidget2 = textWidget2;
	widgetData.statusLabel = statusLabel;
	
	/* callback for search button */
	XtAddCallback(searchButton, XmNactivateCallback, runLocateCommand, &widgetData);
	
	/* add event handler for enter key in search bar */
	XtAddEventHandler(textWidget, KeyPressMask, False, enterKeyPressed, &widgetData);
	
	/* callback for DB update button */
	XtAddCallback(updateButton, XmNactivateCallback, dbCallback, &widgetData);
	
	
	/* close top level */
	XtRealizeWidget(topLevel);
	
	
/* center window on first monitor */
	Display *display = XtDisplay(topLevel);
	Window window = XtWindow(topLevel);
	
	startWindowCentered(topLevel, display, window);

/* make sure meter bar gets repainted in time */
	XSync(display, False);
	XFlush(display);
	
/* enter main processing loop */
	XtAppMainLoop(app);
	
	return 0;
}






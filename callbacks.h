
void instantQuit()
{
	exit(0);
}

void newInstance()
{
	system("dvsearch &");
}

void textWidgetClear(Widget widgetName, XtPointer client_data, XtPointer call_data)
{
	/* get name of widget passed to function */
	char *widgetNameStr = XtName(widgetName);
	
	/* string of nothing */
	char emptyNull[8] = "\0";
	
	/* string compare for names... */
	if(strcmp(widgetNameStr, "clearButton0") == 0)
	{
		/* clear out text box */
		XmTextSetString(textWidget, emptyNull);
		/* give input focus to textWidget */
		XmProcessTraversal(textWidget, XmTRAVERSE_CURRENT);
	}
}

void textWidgetGainedFocus(Widget widgetName, XtPointer client_data, XtPointer call_data)
{
	/* enable visible cursor for insert position */
	XtVaSetValues(widgetName, XmNcursorPositionVisible, 1, NULL);
}

void textWidgetLostFocus(Widget widgetName, XtPointer client_data, XtPointer call_data)
{
	/* disalbles insert cursor */
	XtVaSetValues(widgetName, XmNcursorPositionVisible, 0, NULL);
}


int keepAbove = 0;
void *keepWindowAbove()
{
	while(keepAbove) /* while keepAbove is 1 (true)... */
	{
		/* get Xt display and window */
		Display *display = XtDisplay(topLevel);
		Window window = XtWindow(topLevel);
	
		/* bring window to top of stack */
		XRaiseWindow(display, window);
		/* repaint display */
		XFlush(display);
		
		/* take nap */
		sleep(1);
	}
}

void toggleAlwaysOnTop()
{
	pthread_t keepAboveThread;
	
	if(!keepAbove) /* if keepAbove other windows is false... */
	{
		/* toggle thread flag */
		keepAbove = 1;
		/* spawn another POSIX thread */
		pthread_create(&keepAboveThread, NULL, keepWindowAbove, NULL);
		
		/* set label text on menu entry */
		XtVaSetValues(winEntry0, XtVaTypedArg, XmNlabelString, XmRString, "Stack Normally", 4, NULL);
	}
	
	else if(keepAbove) /* else if true... */
	{
		keepAbove = 0;
		pthread_join(keepAboveThread, NULL); /* removes thread */
		
		XtVaSetValues(winEntry0, XtVaTypedArg, XmNlabelString, XmRString, "Top of Stack", 4, NULL);
	}
}


/* run locate using the data from textWidget */
void runLocateCommand(Widget widget, XtPointer client_data, XtPointer call_data)
{
	char buff[1000000];
	char cmd[1000000];
	XmTextPosition position;
	char* query;

	/* get widget data */
	WidgetData* widgetData = (WidgetData*)client_data;
	Widget textWidget = widgetData->textWidget;
	Widget textWidget2 = widgetData->textWidget2;
	Widget statusLabel = widgetData->statusLabel;
	
	/* update status label */
	XtVaSetValues(statusLabel,
		XtVaTypedArg, XmNlabelString, XmRString, "Searching...", 4,
	NULL);
	
	/* read string data from textWidget*/
	position = XmTextGetLastPosition(textWidget);
	XmTextGetSubstring(textWidget, 0, position, sizeof(buff), buff);
	query = XmTextGetString(textWidget);
	
	
	/* run locate on the string */
	sprintf(cmd, "locate %s\"%s\"", caseSensitivity, query);
	
	/* open pipe for the command */
	FILE* fp = popen(cmd, "r");
	
	if (fp)
	{
		char output[1000000];
		size_t len = fread(output, 1, sizeof(output)-1, fp);
		pclose(fp);
		output[len] = '\0'; /* terminate */
        	
		XtFree(query);
        	/* apply output to textWidget2 */
		XmTextSetString(textWidget2, output);
	}
	
	/* else still free and throw an error */
	else 
	{
		XtFree(query);
		XmTextSetString(textWidget2, "Error executing locate command.");
	}
	
	/* update status label again when done */
	XtVaSetValues(statusLabel,
		XtVaTypedArg, XmNlabelString, XmRString, "Done.", 4,
	NULL);
}


char* fetchOutputFromCmd(const char* command)
{
	#define CMD_BUFFER_SIZE 4096
	
	char* output = NULL;
	char buffer[CMD_BUFFER_SIZE];
	FILE* pipe;

	pipe = popen(command, "r");
	if (!pipe)
	{
		printf("Error running command.\n");
		return NULL;
	}

	/* read cmd output into buffer */
	size_t outputSize = 0;
	while (fgets(buffer, CMD_BUFFER_SIZE, pipe) != NULL) 
	{
		size_t len = strlen(buffer);
		output = realloc(output, outputSize + len + 1);
		
		if (!output) 
		{
			printf("Error.\n");
			return NULL;
		}
		
		strcpy(output + outputSize, buffer);
		outputSize += len;
	}

	/* close pipe */
	pclose(pipe);

	/* terminate output string */
	output = realloc(output, outputSize + 1);
	
	if (!output) 
	{
		printf("Error.\n");
		return NULL;
	}
	output[outputSize] = '\0';
	
	return output;
}


void copyCallback()
{
	system("xdotool key --clearmodifiers Control_L+Insert");
}

void pasteCallback()
{
	system("xdotool key --clearmodifiers Shift_L+Insert");
}


/* on right click */
void rightClickCallback(Widget widget, XtPointer clientData, XEvent* event, Boolean* continueProcessing)
{
	if (event->type == ButtonPress && event->xbutton.button == Button3)
	{
		Widget popupMenu = (Widget)clientData;
		XmMenuPosition(popupMenu, (XButtonPressedEvent*)event);
		XtManageChild(popupMenu);
		*continueProcessing = False;
	}
}


/* on enter key */
void enterKeyPressed(Widget widget, XtPointer client_data, XEvent *event, Boolean *continue_to_dispatch)
{
	if (event->type == KeyPress)
	{
		KeySym keysym;
		char buffer[1];
		int count;

		count = XLookupString(&event->xkey, buffer, sizeof(buffer), &keysym, NULL);
	
		if (count == 1 && buffer[0] == '\r') 
		{
			runLocateCommand(widget, client_data, NULL);
		}
	}
}


/* run with deskview sudo popup */
void dbCallback(Widget widget, XtPointer client_data, XtPointer call_data) 
{
	system("dvsudo updatedb");
}


void toggleCaseSensitivity()
{
	const char ignore_case[32] = "--ignore-case ";
	
	if(strcmp(ignore_case, caseSensitivity) == 0)
	{
		sprintf(caseSensitivity, "\0");
		
		printf("1\n");
		
		XtVaSetValues(optEntry0,
			XtVaTypedArg, XmNlabelString, XmRString, "Case Sensitivity (Enabled)", 4,
		NULL);
	}
	
	else
	{
		sprintf(caseSensitivity, "%s", ignore_case);
		
		printf("0\n");
		
		XtVaSetValues(optEntry0,
			XtVaTypedArg, XmNlabelString, XmRString, "Case Sensitivity (Disabled)", 4,
		NULL);
	}
}


/* attempts to detect window manager via _NET_PROP properties and work around decorations */
int startWindowCentered(Widget widget, Display *display, Window window)
{
/* error handling for lack of a display */
	if(display == NULL)
	{
		fprintf(stderr, "hsvmm: Unable to open display\n");
		fflush(stderr);
		
		return 1;
	}

/* gets atom for _MOTIF_WM_INFO */
	Atom motif_wm_info = XInternAtom(display, "_MOTIF_WM_INFO", True);
	if(motif_wm_info == None)
	{
		return 1;
	}

/* gets _MOTIF_WM_INFO from root window */
	Atom actual_type;
	int actual_format;
	unsigned long nitems, bytes_after;
	unsigned char *prop;
	int status;
	Window root = DefaultRootWindow(display);
	
	status = XGetWindowProperty(display, root, motif_wm_info, 0, 5, False, 
	AnyPropertyType, &actual_type, &actual_format, &nitems, &bytes_after, &prop);
	
/* grab screen count from xinerama */
	int screen_count;
	XineramaScreenInfo *screen_info = XineramaQueryScreens(display, &screen_count);
	
	/* if no screens... */
	if(!screen_info || screen_count == 0)
	{
		fprintf(stderr, "Could not retrieve Xinerama screen info.\n");
		fflush(stderr);
		
		XFree(prop);
		XFree(screen_info);
		return 1;
	}
	
	/* display is topLevel */
	RootWindow(display, screen_info[0].screen_number);
	
/* if it looks like Motif Window Manager is running... */
	if(status == Success && prop != NULL)
	{
		
	/*	FIX LATER - Possibly by detecting _NET_FRAME_EXTENTS 
		----------------------------------------------------
		
		H/W of first Xinerama monitor divided by two. 
		18 and 44 would be the size of the MWM borders 
		and titlebar so divide by 2 again and subtract.
		
		We know the window starts 800x600 so divide by 
		2 again for offset adjustments.
	*/
		int newX = (screen_info[0].width) / 2 - 9 - 400;
		int newY = (screen_info[0].height) / 2 - 22 - 300;
		
		/* move window to new location on first monitor */
		XMoveWindow(display, window, newX, newY);
		
		XFree(prop);
		XFree(screen_info);
		return 1;
	}
	
	else /* if window manager is not MWM... */
	{
		/* attempt to cope with unknown WM decorations by using larger offsets */
		XMoveWindow(display, window, 60, 60);
		
		/* give 120 pixel margin for unknown WM border sizes */
		int newW = screen_info[0].width - 120;
		int newH = screen_info[0].height - 120;
		
		XtVaSetValues(topLevel,
			XmNwidth, newW,
			XmNheight, newH,
		NULL);
		
		XFree(prop);
		XFree(screen_info);
		return 1;
	}
}

























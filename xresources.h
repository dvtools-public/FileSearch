static String fb_xres[] = 
{

/* paper color scheme */


	"dvsearch*foreground: gray10",
	
	"dvsearch*menuBar*background: cornsilk3",
	"dvsearch*menuBar*topShadowColor: #aaa591",
	"dvsearch*menuBar*bottomShadowColor: #716e61",
	
	"dvsearch*textWidget.background: burlywood",
	"dvsearch*textWidget.topShadowColor: #918269",
	"dvsearch*textWidget.bottomShadowColor: #786349",
	
	"dvsearch*XmPushButton.background: cornsilk3",
	"dvsearch*XmPushButton.topShadowColor: #aaa591",
	"dvsearch*XmPushButton.bottomShadowColor: #716e61",
	
	"dvsearch*masterForm.background: cornsilk2",
	"dvsearch*textWidget2*textWidget2SW.background: cornsilk2",
	"dvsearch*statusLabel.background: cornsilk2",
	"dvsearch*dateLabel.background: cornsilk2",
	"dvsearch*XmSeparator.background: cornsilk2",
	
	"dvsearch*textWidget2*XmText.background: #d2be96",
	"dvsearch*textWidget2*XmText.topShadowColor: #eae8de",
	"dvsearch*textWidget2*XmText.bottomShadowColor: #716e61",
	
	"dvsearch*XmScrollBar.background: cornsilk3",
	"dvsearch*XmScrollBar.topShadowColor: #aaa591",
	"dvsearch*XmScrollBar.bottomShadowColor: #716e61",
	"dvsearch*XmScrollBar.troughColor: #d2be96",


	NULL,
};



